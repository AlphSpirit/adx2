package render;

import java.awt.Color;

import maths.Transformation;
import frame.Game;

public abstract class GraphicsRenderer {
	
	public Transformation transform = new Transformation();
	
	public abstract void beginDraw(Game game);
	public abstract void endDraw(Game game);
	
	public abstract void setColor(float r, float g, float b, float a);
	public abstract void setColor(int r, int g, int b, int a);
	public abstract void setColor(Color c);
	
	public abstract void useSurface(Surface s, boolean clear);
	
	public abstract void useShader(Shader shader);
	public abstract void endShape();
	
	public abstract void drawLine(float x1, float y1, float x2, float y2);
	
	public abstract void drawRectangle(float x, float y, float width, float height);
	public abstract void drawRectangleColored(float x, float y, float width, float height, Color c1, Color c2, Color c3, Color c4);
	
	public abstract void drawTexture(Texture texture, float x, float y);
	public abstract void drawTexturePartial(Texture texture, float x, float y, float tx1, float ty1, float tx2, float ty2);
	public abstract void drawTextureSizedPartial(Texture texture, float x, float y, float width, float height, float tx1, float ty1, float tx2, float ty2);
	public abstract void drawTextureColored(Texture texture, float x, float y, Color c1, Color c2, Color c3, Color c4);
	
	public abstract void drawSurface(Surface surface, float x, float y);
	
	public abstract void drawCircle(float x, float y, float radius);
	
	public abstract void drawPolygon(int shape, float[] x, float[] y);

}
