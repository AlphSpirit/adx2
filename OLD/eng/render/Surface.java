package render;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.EXTFramebufferObject.*;

import java.nio.ByteBuffer;

import org.lwjgl.opengl.GL14;

public class Surface {

	private int fboId = -1;
	private int texId = -1;
	private int depthId = -1;
	private int width = -1;
	private int height = -1;

	public Surface(int width, int height) {

		this.width = width;
		this.height = height;

		fboId = glGenFramebuffersEXT();
		texId = glGenTextures();
		depthId = glGenRenderbuffersEXT();

		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fboId);
		
		glBindTexture(GL_TEXTURE_2D, texId);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_INT, (ByteBuffer) null);
		glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, texId, 0);

		glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, depthId);
		glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL14.GL_DEPTH_COMPONENT24, width, height);
		glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, depthId);
		
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

	}
	
	public int getFrameBufferId() {
		return fboId;
	}
	
	public int getTextureId() {
		return texId;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

}
