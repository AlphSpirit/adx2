package render;

import java.awt.Color;

import org.lwjgl.opengl.ARBShaderObjects;

import shapes.CPoint;
import frame.Game;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.EXTFramebufferObject.*;

public class OpenGLDirectRenderer extends GraphicsRenderer {

	private Game game;
	private CPoint p = new CPoint(0, 0);
	private int currentShape = -1;
	private int currentTexture = 0;
	private Shader currentShader = null;
	
	public OpenGLDirectRenderer(Game game) {
		this.game = game;
	}
	
	@Override
	public void beginDraw(Game game) {
		//currentTexture = -1;
	}

	@Override
	public void endDraw(Game game) {
		endShape();
	}

	@Override
	public void setColor(float r, float g, float b, float a) {
		glColor4f(r, g, b, a);
	}
	
	@Override
	public void setColor(int r, int g, int b, int a) {
		glColor4f(r / 255.0f, g / 255.0f, b / 255.0f, a / 255.0f);
	}
	
	@Override
	public void setColor(Color c) {
		glColor4f(c.getRed() / 255.0f, c.getGreen() / 255.0f, c.getBlue() / 255.0f, c.getAlpha() / 255.0f);
	}
	
	@Override
	public void useSurface(Surface s, boolean clear) {
		endShape();
		glBindTexture(GL_TEXTURE_2D, 0);
		currentTexture = 0;
		if (s != null) {
			glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, s.getFrameBufferId());
			glLoadIdentity();
			glOrtho(0, game.getWidth(), 0, game.getHeight(), 1, -1);
		} else {
			glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
			glLoadIdentity();
			glOrtho(0, game.getWidth(), game.getHeight(), 0, 1, -1);
		}
		if (clear) {
			glClear(GL_COLOR_BUFFER_BIT);
		}
	}
	
	@Override
	public void useShader(Shader shader) {
		if (currentShader == shader) {
			return;
		}
		endShape();
		int program;
		if (shader != null) {
			program = shader.getProgram();
		} else {
			program = 0;
		}
		ARBShaderObjects.glUseProgramObjectARB(program);
		currentShader = shader;
	}
	
	@Override
	public void endShape() {
		if (currentShape != -1) {
			glEnd();
			currentShape = -1;
		}
	}
	
	@Override
	public void drawLine(float x1, float y1, float x2, float y2) {
		
		checkTextureAndShape(GL_LINES, 0);
		glVertex2f(x1, y1);
		glVertex2f(x2, y2);
		
	}
	
	@Override
	public void drawRectangle(float x, float y, float width, float height) {
		
		checkTextureAndShape(GL_QUADS, 0);
		glVertex2f(x, y);
		glVertex2f(x + width, y);
		glVertex2f(x + width, y + height);
		glVertex2f(x, y + height);
		
	}
	
	@Override
	public void drawRectangleColored(float x, float y, float width, float height, Color c1, Color c2, Color c3, Color c4) {
		
		checkTextureAndShape(GL_QUADS, 0);
		setColor(c1);
		glVertex2f(x, y);
		setColor(c2);
		glVertex2f(x + width, y);
		setColor(c3);
		glVertex2f(x + width, y + height);
		setColor(c4);
		glVertex2f(x, y + height);
		
	}
	
	@Override
	public void drawTexture(Texture texture, float x, float y) {
		
		checkTextureAndShape(GL_QUADS, texture.getID());
        glTexCoord2f(0, 0);
        vertex(transform.transform(p.setPosition(x, y)));
        glTexCoord2f(1, 0);
        vertex(transform.transform(p.setPosition(x + texture.getWidth(), y)));
        glTexCoord2f(1, 1);
        vertex(transform.transform(p.setPosition(x + texture.getWidth(), y + texture.getHeight())));
        glTexCoord2f(0, 1);
        vertex(transform.transform(p.setPosition(x, y + texture.getHeight())));
		
	}
	
	@Override
	public void drawTexturePartial(Texture texture, float x, float y, float tx1, float ty1, float tx2, float ty2) {
		
		checkTextureAndShape(GL_QUADS, texture.getID());
        glTexCoord2f(tx1, ty1);
        vertex(transform.transform(p.setPosition(x, y)));
        glTexCoord2f(tx2, ty1);
        vertex(transform.transform(p.setPosition(x + texture.getWidth(), y)));
        glTexCoord2f(tx2, ty2);
        vertex(transform.transform(p.setPosition(x + texture.getWidth(), y + texture.getHeight())));
        glTexCoord2f(tx1, ty2);
        vertex(transform.transform(p.setPosition(x, y + texture.getHeight())));
		
	}
	
	@Override
	public void drawTextureSizedPartial(Texture texture, float x, float y, float width, float height, float tx1, float ty1, float tx2, float ty2) {
		
		checkTextureAndShape(GL_QUADS, texture.getID());
        glTexCoord2f(tx1, ty1);
        vertex(transform.transform(p.setPosition(x, y)));
        glTexCoord2f(tx2, ty1);
        vertex(transform.transform(p.setPosition(x + width, y)));
        glTexCoord2f(tx2, ty2);
        vertex(transform.transform(p.setPosition(x + width, y + height)));
        glTexCoord2f(tx1, ty2);
        vertex(transform.transform(p.setPosition(x, y + height)));
		
	}
	
	@Override
	public void drawTextureColored(Texture texture, float x, float y, Color c1, Color c2, Color c3, Color c4) {
		
		checkTextureAndShape(GL_QUADS, texture.getID());
		setColor(c1);
        glTexCoord2f(0, 0);
        vertex(transform.transform(p.setPosition(x, y)));
        setColor(c2);
        glTexCoord2f(1, 0);
        vertex(transform.transform(p.setPosition(x + texture.getWidth(), y)));
        setColor(c3);
        glTexCoord2f(1, 1);
        vertex(transform.transform(p.setPosition(x + texture.getWidth(), y + texture.getHeight())));
        setColor(c4);
        glTexCoord2f(0, 1);
        vertex(transform.transform(p.setPosition(x, y + texture.getHeight())));
		
	}
	
	@Override
	public void drawSurface(Surface surface, float x, float y) {
		
		checkTextureAndShape(GL_QUADS, surface.getTextureId());
		glTexCoord2f(0, 0);
        vertex(transform.transform(p.setPosition(x, y)));
        glTexCoord2f(1, 0);
        vertex(transform.transform(p.setPosition(x + surface.getWidth(), y)));
        glTexCoord2f(1, 1);
        vertex(transform.transform(p.setPosition(x + surface.getWidth(), y + surface.getHeight())));
        glTexCoord2f(0, 1);
        vertex(transform.transform(p.setPosition(x, y + surface.getHeight())));
		
	}
	
	@Override
	public void drawCircle(float x, float y, float radius) {
		
		int circlePrecision = 32;
		float[] px = new float[circlePrecision + 2];
		float[] py = new float[circlePrecision + 2];
		px[0] = x;
		py[0] = y;
		for (int i = 0; i < circlePrecision + 1; i++) {
			px[i + 1] = x + radius * (float) Math.cos(i * Math.PI * 2 / circlePrecision);
			py[i + 1] = y + radius * (float) Math.sin(i * Math.PI * 2 / circlePrecision);
		}
		drawPolygon(GL_TRIANGLE_FAN, px, py);
		
	}
	
	@Override
	public void drawPolygon(int shape, float[] x, float[] y) {
		
		if (x.length != y.length) {
			return;
		}
		checkTextureAndShape(shape, 0);
		int lenght = x.length;
		for (int i = 0; i < lenght; i++) {
			vertex(transform.transform(p.setPosition(x[i], y[i])));
		}
		endShape();
		
	}
	
	private void checkTextureAndShape(int targetShape, int targetTexture) {
		
		if (currentShape != targetShape || currentTexture != targetTexture) {
			if (currentShape != -1 || (currentShape == targetShape && currentTexture != targetTexture && currentTexture != -1)) {
				glEnd();
			}
			if (currentTexture != targetTexture) {
				glBindTexture(GL_TEXTURE_2D, targetTexture);
				currentTexture = targetTexture;
			}
			glBegin(targetShape);
			currentShape = targetShape;
		}
		
	}
	
	private void vertex(CPoint p) {
		glVertex2f(p.x, p.y);
	}

}
