package network;

import java.nio.ByteBuffer;

public class NetData {
	
	private byte[] bytes;
	private ByteBuffer buffer;
	
	public NetData() {
		bytes = new byte[256];
		buffer = ByteBuffer.wrap(bytes);
	}
	
	public NetData(byte[] bytes) {
		this.bytes = bytes;
		buffer = ByteBuffer.wrap(bytes);
	}
	
	public int getPosition() {
		return buffer.position();
	}
	
	public void setPosition(int position) {
		buffer.position(position);
	}
	
	public byte[] getBytes() {
		byte[] r = new byte[buffer.position()];
		System.arraycopy(bytes, 0, r, 0, buffer.position());
		return r;
	}
	
	public byte readByte() {
		return buffer.get();
	}
	
	public short readShort() {
		return buffer.getShort();
	}
	
	public int readInt() {
		return buffer.getInt();
	}
	
	public float readFloat() {
		return buffer.getFloat();
	}
	
	public double readDouble() {
		return buffer.getDouble();
	}
	
	public String readString() {
		int lenght = readShort();
		String str = "";
		for (int i = 0; i < lenght; i++) {
			str += (char) readByte();
		}
		return str;
	}
	
	public void writeByte(byte b) {
		buffer.put(b);
	}
	
	public void writeShort(short s) {
		buffer.putShort(s);
	}
	
	public void writeInt(int i) {
		buffer.putInt(i);
	}
	
	public void writeFloat(float f) {
		buffer.putFloat(f);
	}
	
	public void writeDouble(double d) {
		buffer.putDouble(d);
	}
	
	public void writeString(String s) {
		int lenght = s.length();
		for (int i = 0; i < lenght; i++) {
			writeByte((byte)s.charAt(i));
		}
	}

}
