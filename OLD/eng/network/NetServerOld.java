package network;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

import frame.Game;

public abstract class NetServerOld {
	
	private boolean open = false;
	private Client[] clients;
	private Accept accept;
	private int port;
	
	public NetServerOld(int port, int maxPlayers) {
		
		this.port = port;
		clients = new Client[maxPlayers];
		
	}
	
	public abstract void clientConnected(Client client);
	public abstract void clientDisconnected(Client client);
	public abstract void packetReceived(Client client, NetData data);
	
	public void start() {
		
		if (open) {
			return;
		}
		
		accept = new Accept(port, this);
		accept.start();
		open = true;
		
	}
	
	public void stop() {
		
		if (!open) {
			return;
		}
		
		accept.run = false;
		for (int i = 0; i < clients.length; i++) {
			if (clients[i] != null) {
				killClient(clients[i]);
				clients[i] = null;
			}
		}
		open = false;
		
	}
	
	public Client getClient(int index) {
		return clients[index];
	}
	
	public int getMaxPlayers() {
		return clients.length;
	}
	
	public void broadcast(NetData data) {
		broadcast(data, null);
	}
	
	public void broadcast(NetData data, Client exception) {
		
		Client c;
		for (int i = 0; i < clients.length; i++) {
			c = clients[i];
			if (c != null && c != exception) {
				c.send(data);
			}
		}
		
	}
	
	public void killClient(Client c) {
		c.run = false;
		clients[c.number] = null;
	}
	
	private class Accept extends Thread {
		
		public boolean run = true;
		private NetServerOld server;
		private ServerSocket serverSocket;
		
		public Accept(int port, NetServerOld netServerOld) {
			
			Game.postString("Opening net server on port " + port + "...");
			try {
				serverSocket = new ServerSocket(port);
				serverSocket.setSoTimeout(1000);
			} catch (Exception e) {
				Game.postString("Could not open net server.");
				return;
			}
			this.server = netServerOld;
			Game.postString("Net server successfully opened.");
			
		}
		
		@Override
		public void run() {
			
			Socket socket = null;
			boolean added;
			
			while (run) {
				
				try {
					socket = serverSocket.accept();
					added = false;
					for (int i = 0; i < clients.length; i++) {
						if (clients[i] == null) {
							Client c = new Client(i, socket, server);
							clients[i] = c;
							c.start();
							server.clientConnected(c);
							Game.postString("Client " + i + " " + socket.getInetAddress().getHostAddress() + " connected.");
							added = true;
							break;
						}
					}
					if (!added) {
						Game.postString("Client " + socket.getInetAddress().getHostAddress() + " rejected.");
						socket.close();
					}
				} catch (IOException e) {
				}
				
			}
			
			try {
				if (socket != null) {
					socket.close();
				}
				serverSocket.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			Game.postString("Accept closed.");
			
		}
		
	}
	
	public class Client extends Thread {
		
		public boolean run = true;
		private int number;
		private Socket socket;
		private NetServerOld server;
		private BufferedInputStream is;
		private BufferedOutputStream os;
		
		public Client(int number, Socket socket, NetServerOld server2) {
			
			this.number = number;
			this.socket = socket;
			this.server = server2;
			try {
				this.socket.setSoTimeout(100);
				is = new BufferedInputStream(this.socket.getInputStream());
				os = new BufferedOutputStream(this.socket.getOutputStream());
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
		public int getNumber() {
			return number;
		}
		
		@Override
		public void run() {
			
			byte b;
			
			while (run) {
				
				try {
					b = (byte) is.read();
					if (b != -1) {
						NetData data = new NetData();
						data.writeByte(b);
						while (is.available() > 0) {
							byte by = (byte)is.read();
							data.writeByte(by);
						}
						data.setPosition(0);
						server.packetReceived(this, data);
					} else {
						server.killClient(this);
					}
				} catch (SocketTimeoutException ste) {
				} catch (IOException e) {
					server.killClient(this);
					return;
				}
				
			}
			
			try {
				os.close();
				is.close();
				socket.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			clients[number] = null;
			server.clientDisconnected(this);
			Game.postString("Client " + number + " disconnected.");
			
		}
		
		public void send(NetData data) {
			
			if (data.getPosition() == 0) {
				return;
			}
			try {
				os.write(data.getBytes());
				os.flush();
			} catch (Exception e) {
				server.killClient(this);
				return;
			}
			
		}
		
	}

}
