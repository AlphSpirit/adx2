package network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketTimeoutException;

import frame.Game;

public abstract class NetClientOld {
	
	private boolean connected = false;
	private Server server;
	private String ip;
	private int port;
	
	public NetClientOld(String ip, int port) {
		
		this.ip = ip;
		this.port = port;
		
	}
	
	public boolean isConnected() {
		return connected;
	}
	
	public abstract void packetReceived(Server server, NetData data);
	
	public void connect() {
		
		if (connected) {
			return;
		}
		
		server = new Server(ip, port, this);
		if (server.opened) {
			server.start();
			connected = true;
		}
		
	}
	
	public void disconnect() {
		
		if (!connected) {
			return;
		}
		
		Game.postString("Closing client connection...");
		server.run = false;
		connected = false;
		
	}
	
	public void send(NetData data) {
		server.send(data);
	}
	
	public class Server extends Thread {
		
		private boolean opened = false;
		private boolean run = true;
		private Socket socket;
		private DataInputStream is;
		private DataOutputStream os;
		private NetClientOld client;

		public Server(String ip, int port, NetClientOld client) {
			
			Game.postString("Connecting to " + ip + ":" + port + "...");
			
			try {
				socket = new Socket(ip, port);
				socket.setSoTimeout(100);
				is = new DataInputStream(socket.getInputStream());
				os = new DataOutputStream(socket.getOutputStream());
			} catch (Exception e) {
				run = false;
				return;
			}
			opened = true;
			this.client = client;
			Game.postString("Connection successfully completed.");
			
		}
		
		@Override
		public void run() {
			
			byte b;
			
			while (run) {
				
				try {
					b = (byte) is.read();
					if (b != -1) {
						NetData data = new NetData();
						data.writeByte(b);
						while (is.available() > 0) {
							data.writeByte(is.readByte());
						}
						data.setPosition(0);
						client.packetReceived(this, data);
					}
				} catch (SocketTimeoutException ste) {
				} catch (IOException e) {
					client.disconnect();
					return;
				} 
				
			}
			
			try {
				if (os != null) {
					os.close();
				}
				if (is != null) {
					is.close();
				}
				if (socket != null) {
					socket.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			Game.postString("Client disconnected.");
			
		}
		
		public void send(NetData data) {
			
			try {
				os.write(data.getBytes());
				os.flush();
			} catch (Exception e) {
				client.disconnect();
				return;
			}
			
		}
		
	}

}
