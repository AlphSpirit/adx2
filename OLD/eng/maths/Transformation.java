package maths;

import java.util.ArrayList;

import shapes.CPoint;

public class Transformation {
	
	public static final byte TRANSLATION = 1;
	public static final byte SCALING = 2;
	public static final byte ROTATION = 3;
	public static final byte SHEAR = 4;
	
	private ArrayList<TransformStruct> lTransform = new ArrayList<TransformStruct>();
	
	private class TransformStruct {
		public byte type;
		public float arg1;
		public float arg2;
		public TransformStruct(byte type, float arg1, float arg2) {
			this.type = type;
			this.arg1 = arg1;
			this.arg2 = arg2;
		}
	}
	
	public void translate(float xPlus, float yPlus) {
		lTransform.add(new TransformStruct(TRANSLATION, xPlus, yPlus));
	}
	
	public void scale(float xScale, float yScale) {
		lTransform.add(new TransformStruct(SCALING, xScale, yScale));
	}
	
	public void rotate(float angle) {
		lTransform.add(new TransformStruct(ROTATION, angle, 0));
	}
	
	public void shear(float xShear, float yShear) {
		lTransform.add(new TransformStruct(SHEAR, xShear, yShear));
	}
	
	public void delete() {
		delete(1);
	}
	
	public void delete(int number) {
		for (int i = 0; i < number; i++) {
			if (lTransform.size() > 0) {
				lTransform.remove(lTransform.size() - 1);
			}
		}
	}
	
	public void clear() {
		lTransform.clear();
	}
	
	public CPoint transform(CPoint point) {
		if (lTransform.size() == 0) {
			return point;
		}
		for (TransformStruct t : lTransform) {
			switch (t.type) {
			case TRANSLATION:
				point.x += t.arg1;
				point.y += t.arg2;
				break;
			case SCALING:
				point.x *= t.arg1;
				point.y *= t.arg2;
				break;
			case ROTATION:
				float x = (float)(point.x * Math.cos(t.arg1) - point.y * Math.sin(t.arg1));
				float y = (float)(point.x * Math.sin(t.arg1) + point.y * Math.cos(t.arg1));
				point.x = x;
				point.y = y;
				break;
			case SHEAR:
				break;
			}
		}
		return point;
	}

}
