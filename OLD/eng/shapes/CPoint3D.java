package shapes;

public class CPoint3D {

	public float x;
	public float y;
	public float z;

	public CPoint3D(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public CPoint3D setPosition(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
		return this;
	}

}
