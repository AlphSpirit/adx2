package shapes;

import maths.MathUtils;

public abstract class Collision {
	
	public static boolean getCollision(CRectangle r1, CRectangle r2) {
		return r1.x + r1.width > r2.x && r1.y + r1.height > r2.y && r1.x < r2.x + r2.width && r1.y < r2.y + r2.height;
	}
	
	public static boolean getCollision(CRectangle r, CCircle c) {
		float x = MathUtils.clamp(c.x, r.x, r.x + r.width);
		float y = MathUtils.clamp(c.y, r.y, r.y + r.height);
		return MathUtils.dist(x, y, c.x, c.y) <= c.radius;
	}

}
