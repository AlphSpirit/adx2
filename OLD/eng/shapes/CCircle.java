package shapes;

public class CCircle {
	
	public float x;
	public float y;
	public float radius;
	
	public CCircle(float x, float y, float radius) {
		this.x = x;
		this.y = y;
		this.radius = radius;
	}
	
	public boolean collides(CRectangle r) {
		return Collision.getCollision(r, this);
	}

}
