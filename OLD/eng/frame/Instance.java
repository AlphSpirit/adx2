package frame;

import io.InputManager;
import render.GraphicsRenderer;

public abstract class Instance {

	protected int roomID;
	protected int depth;
	
	public Instance() {
		roomID = -1;
		depth = 0;
	}
	
	public int getRoomID() {
		return roomID;
	}
	
	public void setRoomID(int roomID) {
		this.roomID = roomID;
	}
	
	public int getDepth() {
		return depth;
	}
	
	public abstract void load(Game game);
	public abstract void update(Game game, InputManager input);
	public abstract void render(Game game, GraphicsRenderer g);
	
	public void destroy(Game game) {
		game.removeInstance(this);
	}
	
}
