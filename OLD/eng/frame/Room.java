package frame;

import io.InputManager;
import render.GraphicsRenderer;

public abstract class Room {
	
	private int roomID;
	
	public Room(int roomID) {
		this.roomID = roomID;
	}
	
	public int getRoomID() {
		return roomID;
	}
	
	public abstract void load(Game game);
	public abstract void update(Game game, InputManager input);
	public abstract void render(Game game, GraphicsRenderer g);
	
	public void resize(Game game) {
	}
	
	public void exit(Game game) {
	}

}
