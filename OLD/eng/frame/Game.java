package frame;

import static org.lwjgl.system.MemoryUtil.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import io.InputManager;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Random;

import org.lwjgl.opengl.GLContext;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;
import org.lwjgl.glfw.GLFWWindowSizeCallback;
import org.lwjgl.glfw.GLFWvidmode;

import render.GraphicsRenderer;
import render.OpenGLDirectRenderer;

public class Game {

	public static final int STRETCH_NONE = 0;
	public static final int STRETCH_RESIZE = 1;
	public static final int STRETCH_FULL = 2;
	public static final int STRETCH_SMART = 3;
	public static final int STRETCH_EXPAND = 4;

	private static boolean run = false;
	private long window = -1;
	private int gameWidth;
	private int gameHeight;
	private int displayWidth;
	private int displayHeight;
	private int renderLoops = 0;
	private String gameTitle = "ADX2";
	private Random random = new Random();

	private ArrayList<Room> lRoom = new ArrayList<Room>();
	private Room currentRoom = null;
	private ArrayList<Instance> lInstance = new ArrayList<Instance>();

	private GraphicsRenderer graphics = new OpenGLDirectRenderer(this);
	private InputManager input = new InputManager(this);

	private int stretchMode = STRETCH_FULL;
	private int drawX = 0;
	private int drawY = 0;
	private int drawWidth = 0;
	private int drawHeight = 0;
	
	private GLFWWindowSizeCallback sizeCallback;
	private GLFWKeyCallback keyCallback;
	private GLFWMouseButtonCallback buttonCallback;

	public Game() {
		this(1024, 768);
	}

	public Game(int width, int height) {

		gameWidth = width;
		gameHeight = height;
		displayWidth = width;
		displayHeight = height;

	}

	public Game(int width, int height, String windowTitle) {

		gameWidth = width;
		gameHeight = height;
		displayWidth = width;
		displayHeight = height;
		this.gameTitle = windowTitle;

	}

	public int getWidth() {
		return gameWidth;
	}

	public int getHeight() {
		return gameHeight;
	}

	public int getDisplayWidth() {
		return displayWidth;
	}

	public int getDisplayHeight() {
		return displayHeight;
	}

	public Random getRandom() {
		return random;
	}

	public GraphicsRenderer getGraphics() {
		return graphics;
	}

	public String getTitle() {
		return this.gameTitle;
	}

	public void setTitle(String gameTitle) {
		this.gameTitle = gameTitle;
		if (window != -1) {
			glfwSetWindowTitle(window, gameTitle);
		}
	}
	
	public void setStretchMode(int mode) {
		stretchMode = mode;
	}

	public int getDrawX() {
		return drawX;
	}

	public int getDrawY() {
		return drawY;
	}

	public int getDrawWidth() {
		return drawWidth;
	}

	public int getDrawHeight() {
		return drawHeight;
	}

	private class UpdateThread extends Thread {

		private Game game;
		public boolean run = true;

		public UpdateThread(Game game) {
			this.game = game;
		}

		@Override
		public void run() {

			double up = 0;
			double secTimer = 0;
			double plus;
			long timeNow = System.nanoTime();
			long timeThen = System.nanoTime();

			while (run) {

				timeNow = System.nanoTime();
				plus = (timeNow - timeThen) / 1000000000.0;
				up += plus * 60;
				secTimer += plus;

				if (up >= 1) {

					// Update current room and instances
					synchronized (lInstance) {
						currentRoom.update(game, input);
						Instance instance;
						for (int i = lInstance.size() - 1; i >= 0; i--) {
							instance = lInstance.get(i);
							instance.update(game, input);
						}
					}
					input.update(window);
					up = 0;

				}

				if (secTimer >= 1) {

					postString("FPS: " + renderLoops + " | Instances: " + lInstance.size());
					renderLoops = 0;
					secTimer = 0;

				}

				timeThen = timeNow;

			}

		}

	}

	private class WindowSizeCallback extends GLFWWindowSizeCallback {

		private Game game;

		public WindowSizeCallback(Game game) {
			this.game = game;
		}

		@Override
		public void invoke(long window, int width, int height) {
			game.resize(width, height);
		}

	}

	private class KeyboardCallback extends GLFWKeyCallback {

		private InputManager input;
		
		public KeyboardCallback(InputManager input) {
			this.input = input;
		}

		@Override
		public void invoke(long window, int key, int scancode, int action, int mods) {
			if (action == GLFW_PRESS) {
				input.keyPressed(key);
			} else if (action == GLFW_RELEASE) {
				input.keyReleased(key);
			}
		}

	}
	
	private class MouseButtonCallback extends GLFWMouseButtonCallback {

		private InputManager input;
		
		public MouseButtonCallback(InputManager input) {
			this.input = input;
		}
		
		@Override
		public void invoke(long window, int button, int action, int mods) {
			if (action == GLFW_PRESS) {
				input.buttonPressed(button);
			} else if (action == GLFW_RELEASE) {
				input.buttonReleased(button);
			}
		}
		
	}

	public void start() {

		postString("*** ADX ENGINE v2 - BOOTING UP ***");

		if (currentRoom == null) {
			postError("The game cannot be started without any rooms.");
			stop();
			return;
		}

		postString("Initializing GLFW context...");
		if (glfwInit() != GL_TRUE) {
			postError("Cannot initialize ");
			stop();
			return;
		}

		postString("Setting GLFW flags...");
		glfwDefaultWindowHints();
		glfwWindowHint(GLFW_VISIBLE, GL_FALSE);
		glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

		postString("Creating the GLFW window...");
		window = glfwCreateWindow(displayWidth, displayHeight, this.gameTitle, NULL, NULL);
		if (window == NULL) {
			postError("Cannot create GLFW window.");
			stop();
			return;
		}

		postString("Polling video mode and context...");
		ByteBuffer videoMode = glfwGetVideoMode(glfwGetPrimaryMonitor());
		glfwSetWindowPos(window, (GLFWvidmode.width(videoMode) - displayWidth) / 2, (GLFWvidmode.height(videoMode) - displayHeight) / 2);
		glfwMakeContextCurrent(window);
		glfwSwapInterval(1);
		resize(displayWidth, displayHeight);

		postString("Attaching callbacks...");
		
		glfwSetWindowSizeCallback(window, sizeCallback = new WindowSizeCallback(this));
		glfwSetKeyCallback(window, keyCallback = new KeyboardCallback(input));
		glfwSetMouseButtonCallback(window, buttonCallback = new MouseButtonCallback(input));

		postString("Showing window...");
		glfwShowWindow(window);
		run = true;

		postString("Creating OpenGL context....");
		GLContext.createFromCurrent();
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glOrtho(0, gameWidth, gameHeight, 0, 1, -1);

		postString("Loading starting room...");
		currentRoom.load(this);

		postString("Starting Update thread...");
		UpdateThread upThread = new UpdateThread(this);
		upThread.start();

		postString("Game successfully started.");
		postString("");

		// Main render loop
		while (run && glfwWindowShouldClose(window) == GL_FALSE) {

			glClear(GL_COLOR_BUFFER_BIT);

			graphics.beginDraw(this);

			// Draw current room and instances
			synchronized (lInstance) {
				currentRoom.render(this, graphics);
				for (Instance instance : lInstance) {
					instance.render(this, graphics);
				}
			}

			graphics.endDraw(this);

			glfwSwapBuffers(window);
			glfwPollEvents();

			renderLoops++;

		}

		postString("");
		postString("*** ADX ENGINE v2 - SHUTTING DOWN ***");

		currentRoom.exit(this);

		postString("Joining Update thread...");
		upThread.run = false;
		try {
			upThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		postString("Destroying GLFW window...");
		run = false;
		glfwDestroyWindow(window);

		postString("Game successfully stopped.");

	}

	public void stop() {
		run = false;
	}

	private void resize(int width, int height) {

		displayWidth = width;
		displayHeight = height;

		switch (stretchMode) {

		case STRETCH_NONE:
			drawX = (width - gameWidth) / 2;
			drawY = (height - gameHeight) / 2;
			drawWidth = gameWidth;
			drawHeight = gameHeight;
			break;

		case STRETCH_RESIZE:
			drawX = 0;
			drawY = 0;
			drawWidth = width;
			drawHeight = height;
			gameWidth = width;
			gameHeight = height;
			break;

		case STRETCH_FULL:
			drawX = 0;
			drawY = 0;
			drawWidth = width;
			drawHeight = height;
			break;

		case STRETCH_SMART:
			float x = displayWidth / (float) gameWidth;
			float y = displayHeight / (float) gameHeight;
			if (x < y) {
				drawX = 0;
				drawY = (int) (displayHeight - gameHeight * x) / 2;
				drawWidth = (int) (gameWidth * x);
				drawHeight = (int) (gameHeight * x);
			} else {
				drawX = (int) (displayWidth - gameWidth * y) / 2;
				drawY = 0;
				drawWidth = (int) (gameWidth * y);
				drawHeight = (int) (gameHeight * y);
			}
			break;

		case STRETCH_EXPAND:
			x = displayWidth / (float) gameWidth;
			y = displayHeight / (float) gameHeight;
			if (x > y) {
				drawX = 0;
				drawY = (int) (displayHeight - gameHeight * x) / 2;
				drawWidth = (int) (gameWidth * x);
				drawHeight = (int) (gameHeight * x);
			} else {
				drawX = (int) (displayWidth - gameWidth * y) / 2;
				drawY = 0;
				drawWidth = (int) (gameWidth * y);
				drawHeight = (int) (gameHeight * y);
			}
			break;

		}

		if (run) {
			glLoadIdentity();
			glViewport(drawX, drawY, drawWidth, drawHeight);
			glOrtho(0, gameWidth, gameHeight, 0, 1, -1);
		}

	}

	public void addRoom(Room room) {

		if (currentRoom == null) {
			currentRoom = room;
		}

		lRoom.add(room);

	}

	public void addInstance(Instance instance) {
		boolean added = false;
		if (instance.getRoomID() == -1) {
			instance.setRoomID(currentRoom.getRoomID());
		}
		instance.load(this);
		for (int i = 0; i < lInstance.size(); i++) {
			if (lInstance.get(i).getDepth() < instance.getDepth()) {
				added = true;
				lInstance.add(i, instance);
				break;
			}
		}
		if (!added) {
			lInstance.add(instance);
		}
	}

	public void removeInstance(Instance instance) {
		lInstance.remove(instance);
	}

	public static void postString(String message) {
		System.out.print("      " + message + System.lineSeparator());
	}

	public static void postWarning(String message) {
		System.out.print("[WAR] " + message + System.lineSeparator());
	}

	public static void postError(String message) {
		System.out.print("[ERR] " + message + System.lineSeparator());
	}

}
