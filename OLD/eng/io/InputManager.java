package io;

import static org.lwjgl.glfw.GLFW.*;

import java.nio.DoubleBuffer;

import org.lwjgl.BufferUtils;

import frame.Game;

public class InputManager {
	
	private static final byte RELEASED = 0;
	private static final byte JUST_RELEASED = 1;
	private static final byte PRESSED = 2;
	private static final byte JUST_PRESSED = 3;
	
	private byte[] keyState = new byte[348];
	private byte[] buttonState = new byte[8];
	private int mouseX = 0;
	private int mouseY = 0;
	private Game game;
	
	public InputManager(Game game) {
		this.game = game;
	}
	
	public void keyPressed(int key) {
		keyState[key] = JUST_PRESSED;
	}
	
	public void keyReleased(int key) {
		keyState[key] = JUST_RELEASED;
	}
	
	public void buttonPressed(int button) {
		buttonState[button] = JUST_PRESSED;
	}
	
	public void buttonReleased(int button) {
		buttonState[button] = JUST_RELEASED;
	}
	
	public void update(long window) {
		
		// Keyboard keys state
		for (int i = 0; i < keyState.length; i++) {
			
			if (keyState[i] == JUST_RELEASED) {
				keyState[i] = RELEASED;
			} else if (keyState[i] == JUST_PRESSED) {
				keyState[i] = PRESSED;
			}
			
		}
		
		// Mouse buttons state
		for (int i = 0; i < buttonState.length; i++) {
			
			if (buttonState[i] == JUST_RELEASED) {
				buttonState[i] = RELEASED;
			} else if (buttonState[i] == JUST_PRESSED) {
				buttonState[i] = PRESSED;
			}
			
		}
		
		DoubleBuffer x = BufferUtils.createDoubleBuffer(1);
        DoubleBuffer y = BufferUtils.createDoubleBuffer(1);
        glfwGetCursorPos(window, x, y);
        x.rewind();
        y.rewind();
        mouseX = (int) ((x.get() - game.getDrawX()) * (game.getWidth() / (float) game.getDrawWidth()));
        mouseY = (int) ((y.get() - game.getDrawY()) * (game.getHeight() / (float) game.getDrawHeight()));
		
	}
	
	public boolean getKey(int key) {
		return keyState[key] == PRESSED || keyState[key] == JUST_PRESSED;
	}
	
	public boolean getKeyPressed(int key) {
		return keyState[key] == JUST_PRESSED;
	}
	
	public boolean getKeyReleased(int key) {
		return keyState[key] == JUST_RELEASED;
	}
	
	public boolean getButton(int button) {
		return buttonState[button] == PRESSED || buttonState[button] == JUST_PRESSED;
	}
	
	public boolean getButtonPressed(int button) {
		return buttonState[button] == JUST_PRESSED;
	}
	
	public boolean getButtonReleased(int button) {
		return buttonState[button] == JUST_RELEASED;
	}
	
	public int getMouseX() {
		return mouseX;
	}
	
	public int getMouseY() {
		return mouseY;
	}

}
