package io;

import static org.lwjgl.glfw.GLFW.*;

public abstract class Button {
	
	public static final int LEFT = GLFW_MOUSE_BUTTON_1;
	public static final int RIGHT = GLFW_MOUSE_BUTTON_2;
	public static final int MIDDLE = GLFW_MOUSE_BUTTON_3;
	public static final int B4 = GLFW_MOUSE_BUTTON_4;
	public static final int B5 = GLFW_MOUSE_BUTTON_5;
	public static final int B6 = GLFW_MOUSE_BUTTON_6;
	public static final int B7 = GLFW_MOUSE_BUTTON_7;
	public static final int B8 = GLFW_MOUSE_BUTTON_8;

}
