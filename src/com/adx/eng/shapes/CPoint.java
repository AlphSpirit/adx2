package com.adx.eng.shapes;

public class CPoint {

	public float x;
	public float y;

	public CPoint(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	public CPoint setPosition(float x, float y) {
		this.x = x;
		this.y = y;
		return this;
	}
	
	public boolean collides(CRectangle r) {
		return Collision.getCollision(this, r);
	}

}
