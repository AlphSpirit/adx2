package com.adx.eng.shapes;

public class CRectangle {
	
	public float x;
	public float y;
	public float width;
	public float height;
	
	public CRectangle(float x, float y, float width, float height) {
		
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		
	}
	
	public boolean collides(CPoint p ) {
		return Collision.getCollision(p, this);
	}
	
	public boolean collides(CRectangle r) {
		return Collision.getCollision(this, r);
	}
	
	public boolean collides(CCircle c) {
		return Collision.getCollision(this, c);
	}

}
