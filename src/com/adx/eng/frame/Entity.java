package com.adx.eng.frame;

import java.util.ArrayList;

import com.adx.eng.io.InputManager;
import com.adx.eng.render.GraphicsRenderer;

public abstract class Entity<T extends Instance> extends Instance {
	
	private ArrayList<T> lInstances = new ArrayList<T>();
	
	public void addInstance(Game game, T instance) {
		lInstances.add(instance);
	}
	
	public void removeInstance(Game game, T instance) {
		lInstances.remove(instance);
	}
	
	public T getInstance(int index) {
		return lInstances.get(index);
	}
	
	public ArrayList<T> getInstanceList() {
		return lInstances;
	}

	@Override
	public void load(Game game) {
	}

	@Override
	public void update(Game game, InputManager input) {
	}

	@Override
	public void render(Game game, GraphicsRenderer graphics) {
	}
	
}
