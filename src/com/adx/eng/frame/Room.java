package com.adx.eng.frame;

import com.adx.eng.io.InputManager;
import com.adx.eng.render.GraphicsRenderer;

public abstract class Room {

    private int roomID;

    public Room(int roomID) {
        this.roomID = roomID;
    }

    public int getRoomID() {
        return roomID;
    }

    public abstract void load(Game game);
    public abstract void update(Game game, InputManager input);
    public abstract void render(Game game, GraphicsRenderer g);
    public abstract void exit(Game game);

}
