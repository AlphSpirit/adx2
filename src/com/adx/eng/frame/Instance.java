package com.adx.eng.frame;

import com.adx.eng.io.InputManager;
import com.adx.eng.render.GraphicsRenderer;

public abstract class Instance {

    protected int roomID;
    protected int depth = 0;

    public Instance() {
        roomID = -1;
    }

    public Instance(int roomID) {
        this.roomID = roomID;
    }

    public int getRoomID() {
        return roomID;
    }
    
    public int getDepth() {
    	return depth;
    }

    public void setRoomID(int roomID) {
        this.roomID = roomID;
    }

    public abstract void load(Game game);
    // TODO - Add input manager
    public abstract void update(Game game, InputManager input);
    public abstract void render(Game game, GraphicsRenderer g);

    public void destroy(Game game) {
        game.removeInstance(this);
    }

}
