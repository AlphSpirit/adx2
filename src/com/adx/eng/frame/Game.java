package com.adx.eng.frame;

import com.adx.eng.io.InputManager;
import com.adx.eng.render.GraphicsRenderer;
import com.adx.eng.render.OpenGLLegacyRenderer;

import org.lwjgl.glfw.*;
import org.lwjgl.opengl.GLContext;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Random;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.system.MemoryUtil.*;

public class Game {

	private boolean started = false;
	private long window = -1;
	private static boolean run = true;
	private boolean outputInfos = true;
	private String windowTitle = "ADX Engine V2";

	private int windowWidth;
	private int windowHeight;
	private int gameWidth = windowWidth;
	private int gameHeight = windowHeight;
	private int renderFps = 60;
	private int renderLoops = 0;
	private int viewX = 0;
	private int viewY = 0;

	private InputManager input = new InputManager(this);
	private UpdateThread update;
	private boolean inRenderingThread = false;

	private ArrayList<GraphicsRenderer> lGraphics = new ArrayList<GraphicsRenderer>();
	private GraphicsRenderer currentGraphics = null;
	private Room currentRoom = null;
	private ArrayList<Room> lRoom = new ArrayList<Room>();
	private final ArrayList<Instance> lInstance = new ArrayList<Instance>();
	
	private Random random = new Random();

	@SuppressWarnings("unused")
	private GLFWWindowSizeCallback sizeCallback;
	@SuppressWarnings("unused")
	private GLFWKeyCallback keyCallback;
	@SuppressWarnings("unused")
	private GLFWCharCallback charCallback;
	@SuppressWarnings("unused")
	private GLFWMouseButtonCallback buttonCallback;
	
	private ArrayList<Instance> instancesToLoad = new ArrayList<Instance>();
	private boolean loadRoom = false;

	public Game() {
		gameWidth = 1024;
		gameHeight = 768;
		windowWidth = 1024;
		windowHeight = 768;
	}

	public Game(int width, int height) {
		gameWidth = width;
		gameHeight = height;
		windowWidth = width;
		windowHeight = height;
	}

	/* Accessors / Mutators */

	public int getWidth() {
		return gameWidth;
	}

	public int getHeight() {
		return gameHeight;
	}

	public long getWindow() {
		return window;
	}

	public int getRenderFps() {
		return renderFps;
	}

	public void setRenderFps(int fps) {
		renderFps = fps;
	}

	public InputManager getInput() {
		return input;
	}

	public GraphicsRenderer getGraphics(int index) {
		return lGraphics.get(index);
	}
	
	public void setOutputInfos(boolean output) {
		outputInfos = output;
	}
	
	public Random getRandom() {
		return random;
	}
	
	public int getViewX() {
		return viewX;
	}
	
	public void setViewX(int x) {
		viewX = x;
	}
	
	public void addViewX(int xPlus) {
		viewX += xPlus;
	}
	
	public int getViewY() {
		return viewY;
	}
	
	public void setViewY(int y) {
		viewY = y;
	}
	
	public void addViewY(int yPlus) {
		viewY += yPlus;
	}
	
	public void setTitle(String title) {
		windowTitle = title;
		if (window != -1) {
			glfwSetWindowTitle(window, title);
		}
	}

	private class WindowSizeCallback extends GLFWWindowSizeCallback {
		private Game game;
		public WindowSizeCallback(Game game) {
			this.game = game;
		}
		@Override
		public void invoke(long window, int width, int height) {
			game.resize(width, height);
		}
	}

	private class KeyboardCallback extends GLFWKeyCallback {
		private InputManager input;
		public KeyboardCallback(InputManager input) {
			this.input = input;
		}
		@Override
		public void invoke(long window, int key, int scancode, int action, int mods) {
			if (action == GLFW_PRESS) {
				input.keyPressed(key);
			} else if (action == GLFW_RELEASE) {
				input.keyReleased(key);
			} else if (action == GLFW_REPEAT) {
				input.keyRepeated(key);
			}
		}
	}

	private class KeyboardCharCallback extends GLFWCharCallback {
		private InputManager input;
		public KeyboardCharCallback(InputManager input) {
			this.input = input;
		}
		@Override
		public void invoke(long window, int codepoint) {
			input.addCharToKeyboardString((char) codepoint);
		}
	}

	private class MouseButtonCallback extends GLFWMouseButtonCallback {
		private InputManager input;
		public MouseButtonCallback(InputManager input) {
			this.input = input;
		}
		@Override
		public void invoke(long window, int button, int action, int mods) {
			if (action == GLFW_PRESS) {
				input.buttonPressed(button);
			} else if (action == GLFW_RELEASE) {
				input.buttonReleased(button);
			}
		}
	}

	public void start() {

		if (started) {
			postWarning("You cannot start multiple game instances at once.");
			return;
		}

		postString("ADX ENGINE v2 - INITIALIZING\r\n");

		if (!init()) {
			return;
		}
		render();

		postString("");
		postString("ADX ENGINE v2 - TERMINATING\r\n");

		postString("Joining Update thread...");
		update.run = false;
		try {
			update.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		currentRoom.exit(this);

		postString("Destroying window...");
		glfwDestroyWindow(window);

		postString("Terminating GLFW...");
		glfwTerminate();

		postString("Game successfully stopped.");

	}

	public void stop() {
		run = false;
	}

	private boolean init() {

		if (currentRoom == null) {
			postError("A game must have at least one room.");
			return false;
		}

		postString("Initializing GLFW...");
		if (glfwInit() != GL_TRUE) {
			postError("Initialization of GLFW failed.");
			return false;
		}

		postString("Setting GLFW hints...");
		glfwDefaultWindowHints();
		glfwWindowHint(GLFW_VISIBLE, GL_FALSE);
		glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

		postString("Creating GLFW window...");
		window = glfwCreateWindow(windowWidth, windowHeight, windowTitle, NULL, NULL);
		if (window == NULL) {
			postError("Could not create GLFW window.");
			return false;
		}

		postString("Setting GLFW video mode...");
		ByteBuffer videoMode = glfwGetVideoMode(glfwGetPrimaryMonitor());
		glfwSetWindowPos(window, (GLFWvidmode.width(videoMode) - windowWidth) / 2, (GLFWvidmode.height(videoMode) - windowHeight) / 2);

		postString("Applying GLFW options...");
		glfwMakeContextCurrent(window);
		glfwSwapInterval(1);

		postString("Creating OpenGL context...");
		GLContext.createFromCurrent();
		
		inRenderingThread = true;

		postString("Initializing OpenGL...");
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);

		postString("Creating graphics renderer...");
		if (lGraphics.size() == 0) {
			lGraphics.add(0, new OpenGLLegacyRenderer(this, 0));
		}
		GraphicsRenderer.createWhiteTexture(this);

		postString("Initializing current room...");
		currentRoom.load(this);

		postString("Starting Update thread...");
		update = new UpdateThread(this);
		update.start();

		postString("Attaching callbacks...");
		glfwSetWindowSizeCallback(window, sizeCallback = new WindowSizeCallback(this));
		glfwSetKeyCallback(window, keyCallback = new KeyboardCallback(input));
		glfwSetCharCallback(window, charCallback = new KeyboardCharCallback(input));
		glfwSetMouseButtonCallback(window, buttonCallback = new MouseButtonCallback(input));

		postString("Showing window...");
		glfwShowWindow(window);

		postString("Game successfully started.\r\n");
		started = true;
		
		return true;

	}

	private class UpdateThread extends Thread {

		private Game game;
		public boolean run = true;

		public UpdateThread(Game game) {
			this.game = game;
		}

		@Override
		public void run() {

			double up = 0;
			double secTimer = 0;
			double plus;
			long timeNow;
			long timeThen = System.nanoTime();

			while (run) {

				timeNow = System.nanoTime();
				plus = (timeNow - timeThen) / 1000000000.0;
				up += plus * 60;
				secTimer += plus;

				if (up >= 1) {
					input.updateMousePosition(window);
					// Update current room and instances
					synchronized (lInstance) {
						currentRoom.update(game, input);
						Instance instance;
						for (int i = lInstance.size() - 1; i >= 0; i--) {
							instance = lInstance.get(i);
							instance.update(game, input);
						}
					}
					input.updateButtons(window);
					up = 0;
				}

				if (secTimer >= 1) {
					if (outputInfos) {
						postString("FPS: " + renderLoops + " | Instances: " + lInstance.size());
					}
					renderLoops = 0;
					secTimer = 0;
				}

				timeThen = timeNow;

			}

		}

	}

	private void render() {

		while (glfwWindowShouldClose(window) == GL_FALSE && run) {

			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			
			synchronized (lInstance) {
				inRenderingThread = true;
				if (instancesToLoad.size() > 0) {
					for (Instance instance : instancesToLoad) {
						instance.load(this);
					}
					instancesToLoad.clear();
				}
				if (loadRoom) {
					currentRoom.load(this);
					loadRoom = false;
				}
				for (GraphicsRenderer graphics : lGraphics) {
					if (graphics != currentGraphics) {
						if (currentGraphics != null) {
							currentGraphics.deinit();
						}
						graphics.init();
						currentGraphics = graphics;
					}
					graphics.startDraw();
					currentRoom.render(this, graphics);
					for (Instance instance : lInstance) {
						if (instance.getRoomID() == currentRoom.getRoomID()) {
							instance.render(this, graphics);
						}
					}
					graphics.endDraw();
				}
				inRenderingThread = false;
			}

			glfwSwapBuffers(window);

			glfwPollEvents();

			renderLoops++;

		}

	}
	
	private void resize(int width, int height) {
		// TODO - Mettre le support de resize
	}

	public void addRoom(Room room) {
		lRoom.add(room);
		if (currentRoom == null) {
			currentRoom = room;
		}
	}
	
	public void changeRoom(int roomID) {
		for (Room r : lRoom) {
			if (r.getRoomID() == roomID) {
				currentRoom.exit(this);
				synchronized(lInstance) {
					Instance instance;
					for (int i = lInstance.size() - 1; i >= 0; i--) {
						instance = lInstance.get(i);
						if (instance.getRoomID() != roomID && instance.getRoomID() != -1) {
							lInstance.remove(i);
						}
					}
					currentRoom = r;
					loadRoom = true;
				}
			}
		}
	}

	public void addInstance(Instance instance) {
		boolean added = false;
		if (instance.getRoomID() == -1) {
			instance.setRoomID(currentRoom.getRoomID());
		}
		if (inRenderingThread) {
			instance.load(this);
		} else {
			instancesToLoad.add(instance);
		}
		synchronized(lInstance) {
			for (int i = 0; i < lInstance.size(); i++) {
				if (lInstance.get(i).getDepth() < instance.getDepth()) {
					added = true;
					lInstance.add(i, instance);
					break;
				}
			}
			if (!added) {
				lInstance.add(instance);
			}
		}
	}

	public void removeInstance(Instance instance) {
		lInstance.remove(instance);
	}

	public void addGraphicsRenderer(GraphicsRenderer graphics) {
		this.lGraphics.add(graphics);
	}

	public static void postString(String message) {
		System.out.println("      " + message);
	}

	public static void postWarning(String message) {
		System.out.println("[War] " + message);
	}

	public static void postError(String message) {
		System.out.println("[ERR] " + message);
		run = false;
	}

}
