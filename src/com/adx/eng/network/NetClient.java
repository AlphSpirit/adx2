package com.adx.eng.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import com.adx.eng.frame.Game;

public abstract class NetClient {

	public static final byte PACKET_CONNECT = 100;
	public static final byte PACKET_DISCONNECT = 101;
	public static final byte PACKET_PING = 102;

	private short index = -1;
	private boolean connect = false;
	private InetAddress address;
	private int port;
	private DatagramSocket socket;
	private ListenThread listen;

	public abstract void onConnected();
	public abstract void onPacketReceived(NetData data);
	public abstract void onDisconnected();

	public boolean isConnected() {
		return connect;
	}

	public void connect(String ip, int port) {
		if (connect) {
			return;
		}
		try {
			address = InetAddress.getByName(ip);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		this.port = port;
		Game.postString("Connecting to server " + address.getHostAddress() + ":" + port + "...");
		try {
			socket = new DatagramSocket();
		} catch (SocketException e) {
			Game.postError("Could not connect to the server.");
			return;
		}
		listen = new ListenThread(this);
		listen.start();
		byte[] b = { (byte)0xFF, (byte)0xFF, PACKET_CONNECT };
		DatagramPacket packet = new DatagramPacket(b, b.length, address, port);
		try {
			socket.send(packet);
		} catch (IOException e) {
			Game.postError("Could not send the connection packet.");
			return;
		}
	}

	public void abord() {
		if (connect) {
			return;
		}
		connect = false;
		if (listen != null) {
			listen.run = false;
		}
		if (socket != null) {
			socket.close();
		}
	}
	
	public void disconnect() {
		if (!connect) {
			return;
		}
		connect = false;
		NetData data = new NetData();
		data.writeByte(PACKET_DISCONNECT);
		send(data);
		listen.run = false;
		socket.close();
		Game.postString("Connection successfully closed.");
	}

	public void send(NetData data) {
		if (index == -1 || data.getPosition() == 0) {
			return;
		}
		byte[] b = new byte[data.getPosition() + 2];
		b[0] = (byte) ((index >> 8) & 0xFF);
		b[1] = (byte) (index & 0xFF);
		System.arraycopy(data.getBytes(), 0, b, 2, data.getPosition());
		DatagramPacket packet = new DatagramPacket(b, b.length, address, port);
		try {
			socket.send(packet);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private class ListenThread extends Thread {

		public boolean run = true;
		private NetClient client;

		public ListenThread(NetClient client) {
			this.client = client;
		}

		@Override
		public void run() {

			byte[] buf = new byte[256];
			DatagramPacket packet = new DatagramPacket(buf, buf.length);

			while (run) {

				try {
					socket.receive(packet);
				} catch (IOException e) {
					client.disconnect();
					return;
				}

				NetData data = new NetData(packet.getData(), packet.getLength());
				int type = data.readByte();
				if (type == PACKET_CONNECT) {
					index = data.readShort();
					connect = true;
					Game.postString("Connection successful.");
					onConnected();
				} else if (type == PACKET_DISCONNECT) {
					Game.postError("The server closed the connection.");
					onDisconnected();
				} else if (type == PACKET_PING) {
					NetData d = new NetData();
					d.writeByte(PACKET_PING);
					client.send(d);
				} else {
					data.setPosition(0);
					onPacketReceived(data);
				}

			}

		}

	}

}
