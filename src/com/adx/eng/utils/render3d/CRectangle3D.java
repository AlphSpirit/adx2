package com.adx.eng.utils.render3d;

public class CRectangle3D {
	
	public float x;
	public float y;
	public float z;
	public float width;
	public float height;
	public float depth;
	
	public CRectangle3D(float x, float y, float z, float width, float height, float depth) {
		
		this.x = x;
		this.y = y;
		this.z = z;
		this.width = width;
		this.height = height;
		this.depth = depth;
		if (this.width < 0) {
			this.x += this.width;
			this.width = -this.width;
		}
		if (this.height < 0) {
			this.y += this.height;
			this.height = -this.height;
		}
		
	}
	
	public boolean collides(CRectangle3D r) {
		return this.x + this.width > r.x && this.y + this.height > r.y && this.z + this.depth > r.z && this.x < r.x + r.width && this.y < r.y + r.height && this.z < r.z + r.depth;
	}

}
