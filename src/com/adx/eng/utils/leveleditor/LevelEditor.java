package com.adx.eng.utils.leveleditor;

import java.util.ArrayList;

import javax.swing.JFileChooser;

import com.adx.eng.frame.Game;
import com.adx.eng.frame.Room;
import com.adx.eng.io.Button;
import com.adx.eng.io.IniParser;
import com.adx.eng.io.InputManager;
import com.adx.eng.io.Key;
import com.adx.eng.io.TextFile;
import com.adx.eng.maths.MathUtils;
import com.adx.eng.render.Color;
import com.adx.eng.render.GraphicsRenderer;
import com.adx.eng.render.Texture;
import com.adx.eng.shapes.CPoint;
import com.adx.eng.shapes.CRectangle;

public class LevelEditor extends Room {
	
	private int width;
	private int height;
	private int cellWidth;
	private int cellHeight;
	private int mouseCellX = 0;
	private int mouseCellY = 0;
	private boolean mouseDown = false;
	private int mouseClickX = 0;
	private int mouseClickY = 0;
	
	private int viewSpeed = 8;
	
	private Color backColor;
	private Color roomColor;
	
	private IniParser config;
	
	private ArrayList<ObjectDescription> descriptions;
	private ArrayList<ObjectInstance> instances;
	private int selected = 0;
	private ObjectDescription dSel;
	
	private int backRoomID;
	
	private class ObjectDescription {
		private int id;
		//private String name;
		private Texture tex;
		private int width;
		private int height;
		private boolean scaleX;
		private boolean scaleY;
		public ObjectDescription(int id, String name, Texture tex, int width, int height, boolean scaleX, boolean scaleY) {
			this.id = id;
			//this.name = name;
			this.tex = tex;
			this.width = width;
			this.height = height;
			this.scaleX = scaleX;
			this.scaleY = scaleY;
		}
	}
	
	public class ObjectInstance {
		private ObjectDescription description;
		private int x;
		private int y;
		private int width;
		private int height;
		private CRectangle r;
		public ObjectInstance(ObjectDescription description, int x, int y) {
			this(description, x, y, description.width, description.height);
		}
		public ObjectInstance(ObjectDescription description, int x, int y, int width, int height) {
			this.description = description;
			this.x = x;
			this.y = y;
			this.width = width;
			this.height = height;
			r = new CRectangle(x, y, width, height);
		}
	}
	
	public LevelEditor(int roomID, String configPath, int backRoomID) {
		
		super(roomID);
		config = new IniParser(configPath);
		this.backRoomID = backRoomID;
		
	}

	@Override
	public void load(Game game) {
		
		descriptions = new ArrayList<ObjectDescription>();
		// Load configuration
		int objectNumber = config.getIntValue("GLOBAL", "objectNumber");
		// Global and room config
		width = config.getIntValue("GLOBAL", "width");
		height = config.getIntValue("GLOBAL", "height");
		cellWidth = config.getIntValue("GLOBAL", "cellWidth");
		cellHeight = config.getIntValue("GLOBAL", "cellHeight");
		backColor = new Color(
				config.getFloatValue("GLOBAL", "backgroundRed"),
				config.getFloatValue("GLOBAL", "backgroundGreen"),
				config.getFloatValue("GLOBAL", "backgroundBlue")
				);
		roomColor = new Color(
				config.getFloatValue("GLOBAL", "roomRed"),
				config.getFloatValue("GLOBAL", "roomGreen"),
				config.getFloatValue("GLOBAL", "roomBlue")
				);
		// Objects config
		for (int i = 1; i <= objectNumber; i++) {
			descriptions.add(new ObjectDescription(
					config.getIntValue("Object" + i, "id"),
					config.getValue("Object" + i, "name"),
					new Texture(config.getValue("Object" + i, "texture")),
					config.getIntValue("Object" + i, "width"),
					config.getIntValue("Object" + i, "height"),
					config.getIntValue("Object" + i, "scaleX") == 1,
					config.getIntValue("Object" + i, "scaleY") == 1));
		}
		instances = new ArrayList<ObjectInstance>();
		dSel = descriptions.get(selected);
		game.setTitle("Level Editor!");
		
	}

	@Override
	public void update(Game game, InputManager input) {
		
		if (input.getKeyPressed(Key.ESCAPE)) {
			game.changeRoom(backRoomID);
			return;
		}
		
		// Move camera
		if (input.getKey(Key.D)) {
			game.addViewX(viewSpeed);
		}
		if (input.getKey(Key.A)) {
			game.addViewX(-viewSpeed);
		}
		if (input.getKey(Key.S)) {
			game.addViewY(viewSpeed);
		}
		if (input.getKey(Key.W)) {
			game.addViewY(-viewSpeed);
		}
		
		// Change selected object
		if (input.getKeyPressed(Key.D1)) {
			selected = 0;
			dSel = descriptions.get(selected);
		}
		if (input.getKeyPressed(Key.D2)) {
			selected = 1;
			dSel = descriptions.get(selected);
		}
		if (input.getKeyPressed(Key.D3)) {
			selected = 2;
			dSel = descriptions.get(selected);
		}
		
		mouseCellX = (input.getMouseX() + game.getViewX()) / cellWidth;
		if (input.getMouseX() + game.getViewX() < 0) {
			mouseCellX--;
		}
		mouseCellY = (input.getMouseY() + game.getViewY()) / cellHeight;
		if (input.getMouseY() + game.getViewY() < 0) {
			mouseCellY--;
		}
		
		// Place instances
		if (!dSel.scaleX && !dSel.scaleY) {
			
			if (input.getButton(Button.LEFT)) {
				// Test collision with other instances
				boolean place = true;
				CPoint p = new CPoint(mouseCellX, mouseCellY);
				for (ObjectInstance instance : instances) {
					if (p.collides(instance.r)) {
						place = false;
						break;
					}
				}
				if (place) {
					instances.add(new ObjectInstance(dSel, mouseCellX, mouseCellY));
				}
			}
			
		} else {
		
			if (!mouseDown && input.getButtonPressed(Button.LEFT)) {
				mouseDown = true;
				mouseClickX = mouseCellX;
				mouseClickY = mouseCellY;
			}
			if(mouseDown && input.getButtonReleased(Button.LEFT)) {
				mouseDown = false;
				// Place instance
				int instanceX = 0;
				int instanceY = 0;
				int instanceWidth;
				if (dSel.scaleX) {
					instanceWidth = (int) MathUtils.max(Math.abs(mouseClickX - mouseCellX) + 1, dSel.width);
					instanceX = (int) MathUtils.min(mouseClickX, mouseCellX);
				} else {
					instanceWidth = dSel.width;
					instanceX = mouseClickX;
				}
				int instanceHeight;
				if (dSel.scaleY) {
					instanceHeight = (int) MathUtils.max(Math.abs(mouseClickY - mouseCellY) + 1, dSel.height);
					instanceY = (int) MathUtils.min(mouseClickY, mouseCellY);
				} else {
					instanceHeight = dSel.height;
					instanceY = mouseClickY;
				}
				// Test collisions
				CRectangle r = new CRectangle(instanceX, instanceY, instanceWidth, instanceHeight);
				boolean place = true;
				for (ObjectInstance instance : instances) {
					if (r.collides(instance.r)) {
						place = false;
						break;
					}
				}
				if (place) {
					instances.add(new ObjectInstance(dSel, instanceX, instanceY, instanceWidth, instanceHeight));
				}
			}
		
		}
		
		// Remove instances
		if (input.getButton(Button.RIGHT)) {
			
		}
		
		// Save file
		if (input.getKeyPressed(Key.S) && input.getKey(Key.LEFT_CONTROL)) {
			JFileChooser chooser = new JFileChooser();
			int result = chooser.showSaveDialog(null);
			if (result == JFileChooser.APPROVE_OPTION) {
				String path = chooser.getSelectedFile().getAbsolutePath();
				outputToFile(path);
			}
		}
		
	}

	@Override
	public void render(Game game, GraphicsRenderer g) {
		
		// Draw background
		g.setColor(backColor);
		g.drawRectangle(game.getViewX(), game.getViewY(), game.getWidth(), game.getHeight());
		g.setColor(roomColor);
		g.drawRectangle(0, 0, width, height);
		
		// Draw grid
		g.setColor(0, 0, 0, 85);
		for (int x = 0; x < width / cellWidth; x++) {
			g.drawLine(x * cellWidth, 0, x * cellWidth, height);
		}
		for (int y = 0; y < height / cellHeight; y++) {
			g.drawLine(0, y * cellHeight, width, y * cellHeight);
		}
		
		// Render all instances
		g.setColor(255, 255, 255, 255);
		for (ObjectInstance instance : instances) {
			g.transform.scaleFrom(instance.x * cellWidth, instance.y * cellHeight, instance.width, instance.height);
			g.drawTexture(instance.description.tex, instance.x * cellWidth, instance.y * cellHeight);
			g.transform.remove(3);
		}
		
		// Render placement rectangle
		if (mouseDown) {
			int recX = 0;
			int recY = 0;
			int recWidth;
			if (dSel.scaleX) {
				recWidth = (int) MathUtils.max(Math.abs(mouseClickX - mouseCellX) + 1, dSel.width);
				recX = (int) MathUtils.min(mouseClickX, mouseCellX);
			} else {
				recWidth = dSel.width;
				recX = mouseClickX;
			}
			int recHeight;
			if (dSel.scaleY) {
				 recHeight = (int) MathUtils.max(Math.abs(mouseClickY - mouseCellY) + 1, dSel.height);
				 recY = (int) MathUtils.min(mouseClickY, mouseCellY);
			} else {
				recHeight = dSel.height;
				recY = mouseClickY;
			}
			g.setColor(0, 255, 0, 85);
			g.drawRectangle(recX * cellWidth, recY * cellHeight, recWidth * cellWidth, recHeight * cellHeight);
		}
		
		// Draw click cursor
		if (mouseDown) {
			g.setColor(255, 255, 255, 85);
			g.drawRectangle(mouseClickX * cellWidth, mouseClickY * cellHeight, cellWidth, cellHeight);
		}
		
		// Draw selection cursor
		g.setColor(255, 255, 255, 85);
		g.drawRectangle(mouseCellX * cellWidth, mouseCellY * cellHeight, cellWidth, cellHeight);
		
	}

	@Override
	public void exit(Game game) {
	}
	
	private void outputToFile(String path) {
		
		TextFile file = new TextFile(path);
		file.clearLines();
		
		// Write instances
		file.insertLine("*INSTANCES*");
		for (ObjectInstance instance : instances) {
			file.insertLine(instance.description.id + ";"
					+ instance.x + ";"
					+ instance.y + ";"
					+ instance.width + ";"
					+ instance.height);
		}
		file.close();
		
	}

}
