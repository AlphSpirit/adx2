package com.adx.eng.maths;

import java.util.ArrayList;

public class Transform {
	
	public static final int TRANSLATION = 1;
	public static final int SCALING = 2;
	public static final int ROTATION = 3;
	
	public ArrayList<Transformation> transformations = new ArrayList<Transformation>();
	
	private class Transformation {
		private int type;
		private float arg1;
		private float arg2;
		public Transformation(int type, float arg1, float arg2) {
			this.type = type;
			this.arg1 = arg1;
			this.arg2 = arg2;
		}
	}
	
	public void remove() {
		transformations.remove(transformations.size() - 1);
	}
	
	public void remove(int quantity) {
		for (int i = 0; i < quantity; i++) {
			remove();
		}
	}
	
	public void clear() {
		transformations.clear();
	}
	
	public void translate(float xPlus, float yPlus) {
		transformations.add(new Transformation(TRANSLATION, xPlus, yPlus));
	}
	
	public void scale(float xScale, float yScale) {
		transformations.add(new Transformation(SCALING, xScale, yScale));
	}
	
	public void rotate(float angle) {
		transformations.add(new Transformation(ROTATION, angle, 0));
	}
	
	public void scaleFrom(float x, float y, float xScale, float yScale) {
		translate(-x, -y);
		scale(xScale, yScale);
		translate(x, y);
	}
	
	public void rotateFrom(float x, float y, float angle) {
		translate(-x, -y);
		rotate(angle);
		translate(x, y);
	}
	
	public Vector applyTransformations(Vector v) {
		
		for (Transformation t : transformations) {
			
			switch (t.type) {
			
			case TRANSLATION:
				v.x += t.arg1;
				v.y += t.arg2;
				break;
				
			case SCALING:
				v.x *= t.arg1;
				v.y *= t.arg2;
				break;
				
			case ROTATION:
				float nX = (float) (v.x * Math.cos(t.arg1) - v.y * Math.sin(t.arg1));
				float nY = (float) (v.x * Math.sin(t.arg1) + v.y * Math.cos(t.arg1));
				v.x = nX;
				v.y = nY;
				break;
			
			}
			
		}
		
		return v;
		
	}

}
