package com.adx.eng.maths;

/**
 * Mathematics utilities abstract class.
 * Contains various algorithms and formulas that are generic to game development.
 * @author Alexandre Desbiens
 */
public abstract class MathUtils {

	/**
	 * Returns the distance from the origin to the given point.
	 * @param x Point X
	 * @param y Point Y
	 * @return Distance between origin and point
	 */
	public static float dist(float x, float y) {
		return (float) Math.sqrt(Math.pow(y, 2) + Math.pow(x, 2));
	}

	/**
	 * Returns the distance between the two given points.
	 * @param x1 Point 1 X
	 * @param y1 Point 1 Y
	 * @param x2 Point 2 X
	 * @param y2 Point 2 Y
	 * @return Distance between the 2 points
	 */
	public static float dist(float x1, float y1, float x2, float y2) {
		return (float) Math.sqrt(Math.pow(y2 - y1, 2) + Math.pow(x2 - x1, 2));
	}

	/**
	 * Returns the distance between the two given 3D points.
	 * @param x1 Point 1 X
	 * @param y1 Point 1 Y
	 * @param z1 Point 1 Z
	 * @param x2 Point 2 X
	 * @param y2 Point 2 Y
	 * @param z2 Point 2 Z
	 * @return
	 */
	public static float dist(float x1, float y1, float z1, float x2, float y2, float z2) {
		return (float) Math.sqrt(Math.pow(z2 - z1, 2) + Math.pow(y2 - y1, 2) + Math.pow(x2 - x1, 2));
	}

	/**
	 * Returns the angle between the origin and the given point.
	 * @param x Point X
	 * @param y Point Y
	 * @return Angle between origin and point
	 */
	public static float angle(float x, float y) {
		return (float) Math.atan2(y, x);
	}

	/**
	 * Returns the angle between the two given points.
	 * @param x1 Point 1 X
	 * @param y1 Point 1 Y
	 * @param x2 Point 2 X
	 * @param y2 Point 2 Y
	 * @return Angle between the two points
	 */
	public static float angle(float x1, float y1, float x2, float y2) {
		return (float) Math.atan2(y2 - y1, x2 - x1);
	}

	/**
	 * Gives the minimum value contained in the arguments.
	 * @param list Values
	 * @return Minimum value
	 */
	public static float min(float... list) {
		float min = Float.MAX_VALUE;
		for (float x : list) {
			if (x < min) {
				min = x;
			}
		}
		return min;
	}

	/**
	 * Gives the maximum value contained in the arguments.
	 * @param list Values
	 * @return Maximum value
	 */
	public static float max(float... list) {
		float max = Float.MIN_VALUE;
		for (float x : list) {
			if (x > max) {
				max = x;
			}
		}
		return max;
	}

	/**
	 * Returns the average of all the values given.
	 * @param list Values
	 * @return Average of the values
	 */
	public static float avrg(float... list) {
		float add = 0;
		for (float x : list) {
			add += x;
		}
		return add / list.length;
	}
	
	/**
	 * Limits a value between two bounds.
	 * @param value Value
	 * @param min Lower bound
	 * @param max Upper bound
	 * @return Clamped value
	 */
	public static float clamp(float value, float min, float max) {
		if (value > max) {
			return max;
		} else if (value < min) {
			return min;
		} else {
			return value;
		}
	}

}
