package com.adx.eng.maths;

public class Vector {
	
	public float x;
	public float y;
	
	public Vector(float x, float y) {
		setPosition(x, y);
	}
	
	public void setPosition(float x, float y) {
		this.x = x;
		this.y = y;
	}

}
