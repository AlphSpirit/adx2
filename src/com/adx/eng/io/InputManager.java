package com.adx.eng.io;

import java.nio.DoubleBuffer;

import com.adx.eng.frame.Game;
import org.lwjgl.BufferUtils;

import static org.lwjgl.glfw.GLFW.*;

public class InputManager {

	private static final byte RELEASED = 0;
	private static final byte JUST_RELEASED = 1;
	private static final byte PRESSED = 2;
	private static final byte JUST_PRESSED = 3;

	private byte[] keyState = new byte[348];
	private byte[] buttonState = new byte[8];
	private int mouseX = 0;
	private int mouseY = 0;
	private Game game;
	private String keyboardString = "";
	private int keyboardStringLimit = -1;
	private int keyboardStringIndex = 0;

	public InputManager(Game game) {
		this.game = game;
	}

	public void keyPressed(int key) {
		if (key >= 0 && key < keyState.length) {
			keyState[key] = JUST_PRESSED;
			updateKeyboardString(key);
		}
	}

	public void keyReleased(int key) {
		if (key >= 0 && key < keyState.length) {
			keyState[key] = JUST_RELEASED;
		}
	}
	
	public void keyRepeated(int key) {
		if (key >= 0 && key < keyState.length) {
			updateKeyboardString(key);
		}
	}
	
	public void updateKeyboardString(int key) {
		if (key == Key.BACKSPACE && keyboardString.length() > 0 && keyboardStringIndex > 0) {
			keyboardString = keyboardString.substring(0, keyboardStringIndex - 1) + keyboardString.substring(keyboardStringIndex, keyboardString.length());
			keyboardStringIndex--;
		} else if (key == Key.RIGHT && keyboardStringIndex < keyboardString.length()) {
			keyboardStringIndex++;
		} else if (key == Key.LEFT && keyboardStringIndex > 0) {
			keyboardStringIndex--;
		}
	}

	public void releaseKey(int key) {
		if (keyState[key] == JUST_RELEASED) {
			keyState[key] = RELEASED;
		} else if (keyState[key] == JUST_PRESSED) {
			keyState[key] = PRESSED;
		}
	}

	public void releaseButton(int button) {
		if (buttonState[button] == JUST_RELEASED) {
			buttonState[button] = RELEASED;
		} else if (buttonState[button] == JUST_PRESSED) {
			buttonState[button] = PRESSED;
		}
	}

	public void buttonPressed(int button) {
		buttonState[button] = JUST_PRESSED;
	}

	public void buttonReleased(int button) {
		buttonState[button] = JUST_RELEASED;
	}

	public void updateButtons(long window) {

		// Keyboard keys state
		for (int i = 0; i < keyState.length; i++) {

			if (keyState[i] == JUST_RELEASED) {
				keyState[i] = RELEASED;
			} else if (keyState[i] == JUST_PRESSED) {
				keyState[i] = PRESSED;
			}

		}

		// Mouse buttons state
		for (int i = 0; i < buttonState.length; i++) {

			if (buttonState[i] == JUST_RELEASED) {
				buttonState[i] = RELEASED;
			} else if (buttonState[i] == JUST_PRESSED) {
				buttonState[i] = PRESSED;
			}

		}

	}

	public void updateMousePosition(long window) {

		DoubleBuffer x = BufferUtils.createDoubleBuffer(1);
		DoubleBuffer y = BufferUtils.createDoubleBuffer(1);
		glfwGetCursorPos(window, x, y);
		x.rewind();
		y.rewind();
		// TODO - Faire le placement de souris comme du monde
		mouseX = (int) x.get();
		mouseY = (int) y.get();

	}

	public String getKeyboardString() {
		return keyboardString;
	}

	public void setKeyboardString(String str) {
		keyboardString = str;
		keyboardStringIndex = str.length();
	}
	
	public void addCharToKeyboardString(char c) {
		if (keyboardStringLimit == - 1 || keyboardString.length() < keyboardStringLimit) {
			keyboardString = keyboardString.substring(0, keyboardStringIndex) + c + keyboardString.substring(keyboardStringIndex, keyboardString.length());
			keyboardStringIndex++;
		}
	}
	
	public int getKeyboardStringIndex() {
		return keyboardStringIndex;
	}
	
	public void setKeyboardStringLimit(int limit) {
		keyboardStringLimit = limit;
	}

	public boolean getKey(int key) {
		return keyState[key] == PRESSED || keyState[key] == JUST_PRESSED;
	}

	public boolean getKeyPressed(int key) {
		return keyState[key] == JUST_PRESSED;
	}

	public boolean getKeyReleased(int key) {
		return keyState[key] == JUST_RELEASED;
	}

	public boolean getButton(int button) {
		return buttonState[button] == PRESSED || buttonState[button] == JUST_PRESSED;
	}

	public boolean getButtonPressed(int button) {
		return buttonState[button] == JUST_PRESSED;
	}

	public boolean getButtonReleased(int button) {
		return buttonState[button] == JUST_RELEASED;
	}

	public int getMouseX() {
		return mouseX;
	}

	public int getMouseY() {
		return mouseY;
	}

	public void setMousePosition(int x, int y) {
		glfwSetCursorPos(game.getWindow(), x, y);
		mouseX = x;
		mouseY = y;
	}

	public void setMouseVisible(boolean visible) {
		if (visible) {
			glfwSetInputMode(game.getWindow(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		} else {
			glfwSetInputMode(game.getWindow(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		}
	}
	
	public void disableMouse() {
		disableMousePosition();
		for (int i = 0; i < buttonState.length; i++) {
			buttonState[i] = RELEASED;
		}
	}
	
	public void disableMousePosition() {
		mouseX = Integer.MIN_VALUE;
		mouseY = Integer.MIN_VALUE;
	}
	
	public void disableKeyboard() {
		for (int i = 0; i < keyState.length; i++) {
			keyState[i] = RELEASED;
		}
	}

}
