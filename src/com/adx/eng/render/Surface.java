package com.adx.eng.render;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.EXTFramebufferObject.*;

import java.nio.ByteBuffer;

public class Surface {

	private int fboID = -1;
	private int texID = -1;
	private int width = -1;
	private int height = -1;


	public Surface(int width, int height) {

		this.width = width;
		this.height = height;

		fboID = glGenFramebuffersEXT();
		texID = glGenTextures();

		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fboID);
		
		glBindTexture(GL_TEXTURE_2D, texID);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_INT, (ByteBuffer) null);
		glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, texID, 0);
		
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

	}
	
	public int getFrameBufferID() {
		return fboID;
	}
	
	public int getTextureID() {
		return texID;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

    public void bind(GraphicsRenderer graphics, boolean clear) {
        glBindTexture(GL_TEXTURE_2D, 0);
        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fboID);
        if (clear) {
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        }
    }

    public void unbind(GraphicsRenderer graphics) {
        glBindTexture(GL_TEXTURE_2D, 0);
        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
    }

}
