package com.adx.eng.render;

import org.lwjgl.opengl.ARBShaderObjects;

import com.adx.eng.frame.Game;
import com.adx.eng.maths.Vector;

import static org.lwjgl.opengl.GL11.*;

public class OpenGLLegacyRenderer extends GraphicsRenderer {
	
	public int currentShape = 0;
	public int currentTexture = -1;
	public Shader currentShader = null;
	private Vector v = new Vector(0, 0);
	
	public OpenGLLegacyRenderer(Game game, int id) {
		super(game, id);
	}

	@Override
	public void init() {

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glViewport(0, 0, game.getWidth(), game.getHeight());
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glOrtho(game.getViewX(), game.getWidth() + game.getViewX(), game.getHeight() + game.getViewY(), game.getViewY(), 1, -1);

	}
	
	@Override
	public void deinit() {
	}
	
	@Override
	public void startDraw() {
		super.startDraw();
		currentTexture = -1;
	}
	
	@Override
	public void endDraw() {
		endShape();
		super.endDraw();
	}

	@Override
	public void setColor(float r, float g, float b, float a) {
		glColor4f(r, g, b, a);
	}

	@Override
	public void setColor(int r, int g, int b, int a) {
		glColor4f(r / 255.0f, g / 255.0f, b / 255.0f, a / 255.0f);
	}

	@Override
	public void setColor(Color c) {
		glColor4f(c.r, c.g, c.b, c.a);
	}
	
	public void setShader(Shader shader) {
		if (currentShader == shader) {
			return;
		}
		endShape();
		int program;
		if (shader != null) {
			program = shader.getProgram();
		} else {
			program = 0;
		}
		ARBShaderObjects.glUseProgramObjectARB(program);
		currentShader = shader;
	}

	// Outlines
	
	@Override
	public void outlineRectangle(float x, float y, float width, float height) {
		
		checkShapeTexture(GL_LINES, 0);
		glVertex2f(x, y);
		glVertex2f(x + width, y + 1);
		glVertex2f(x + width, y);
		glVertex2f(x + width, y + height);
		glVertex2f(x, y + height);
		glVertex2f(x + width, y + height - 1);
		glVertex2f(x + 1, y);
		glVertex2f(x + 1, y + height);
		
	}
	
	@Override
	public void outlineRectangle(float x, float y, float width, float height, Color c) {
		setColor(c);
		outlineRectangle(x, y, width, height);
	}
	
	// Filled shapes
	
	@Override
	public void drawLine(float x1, float y1, float x2, float y2) {
		
		checkShapeTexture(GL_LINES, 0);
		glVertex2f(x1, y1);
		glVertex2f(x2, y2);
		
	}
	
	@Override
	public void drawLine(float x1, float y1, float x2, float y2, Color c) {
		setColor(c);
		drawLine(x1, y1, x2, y2);
	}
	
	@Override
	public void drawRectangle(float x, float y, float width, float height) {

		checkShapeTexture(GL_QUADS, 0);
		glVertex2f(x, y);
		glVertex2f(x + width, y);
		glVertex2f(x + width, y + height);
		glVertex2f(x, y + height);

	}
	
	@Override
	public void drawRectangle(float x, float y, float width, float height, Color c) {
		setColor(c);
		drawRectangle(x, y, width, height);
	}
	
	@Override
	public void drawRectangleColored(float x, float y, float width, float height, Color c1, Color c2, Color c3, Color c4) {
		
		checkShapeTexture(GL_QUADS, 0);
		setColor(c1);
		glVertex2f(x, y);
		setColor(c2);
		glVertex2f(x + width, y);
		setColor(c3);
		glVertex2f(x + width, y + height);
		setColor(c4);
		glVertex2f(x, y + height);
		
	}
	
	@Override
	public void drawCircle(float x, float y, float radius) {
		
		int precision = 32;
		checkShapeTexture(GL_TRIANGLE_FAN, 0);
		glVertex2f(x, y);
		float angle;
		for (int i = 0; i < precision + 1; i++) {
			angle = (float) Math.PI * 2 * (i / (float) precision);
			glVertex2f(x + radius * (float) Math.cos(angle), y + radius * (float) Math.sin(angle));
		}
		endShape();
		
	}
	
	@Override
	public void drawCircle(float x, float y, float radius, Color c) {
		setColor(c);
		drawCircle(x, y, radius);
	}
	
	// Fonts
	
	public void drawText(AngelCodeFont font, String text, float x, float y) {
		font.draw(this, text, x, y);
	}
	
	@Override
	public void drawText(AngelCodeFont font, String text, float x, float y, Color c) {
		setColor(c);
		drawText(font, text, x, y);
	}
	
	// Textures and surfaces

	@Override
	public void drawTexture(Texture texture, float x, float y) {

		checkShapeTexture(GL_QUADS, texture.getID());
		glTexCoord2f(0, 0);
		vertex(x, y);
		glTexCoord2f(1, 0);
		vertex(x + texture.getWidth(), y);
		glTexCoord2f(1, 1);
		vertex(x + texture.getWidth(), y + texture.getHeight());
		glTexCoord2f(0, 1);
		vertex(x, y + texture.getHeight());

	}
	
	@Override
	public void drawTexture(Texture texture, float x, float y, Color c) {
		setColor(c);
		drawTexture(texture, x, y);
	}

	@Override
	public void drawTextureSizedPartial(Texture texture, float x, float y, float width, float height, float tx1, float ty1, float tx2, float ty2) {

		checkShapeTexture(GL_QUADS, texture.getID());
		glTexCoord2f(tx1, ty1);
		glVertex2f(x, y);
		glTexCoord2f(tx2, ty1);
		glVertex2f(x + width, y);
		glTexCoord2f(tx2, ty2);
		glVertex2f(x + width, y + height);
		glTexCoord2f(tx1, ty2);
		glVertex2f(x, y + height);

	}
	
	@Override
	public void drawTextureSizedPartial(Texture texture, float x, float y, float width, float height, float tx1, float ty1, float tx2, float ty2, Color c) {
		setColor(c);
		drawTextureSizedPartial(texture, x, y, width, height, tx1, ty1, tx2, ty2);
	}

	@Override
	public void drawSurface(Surface surface, float x, float y) {

		checkShapeTexture(GL_QUADS, surface.getTextureID());
		glTexCoord2f(0, 0);
		glVertex2f(x, y + surface.getHeight());
		glTexCoord2f(1, 0);
		glVertex2f(x + surface.getWidth(), y + surface.getHeight());
		glTexCoord2f(1, 1);
		glVertex2f(x + surface.getWidth(), y);
		glTexCoord2f(0, 1);
		glVertex2f(x, y);

	}
	
	@Override
	public void drawSurface(Surface surface, float x, float y, Color c) {
		setColor(c);
		drawSurface(surface, x, y);
	}
	
	public void checkShapeTexture(int shape, int texture) {
		if (currentShape == shape && currentTexture == texture) {
			return;
		}
		glEnd();
		if (currentTexture != texture) {
			currentTexture = texture;
			glBindTexture(GL_TEXTURE_2D, texture);
		}
		glBegin(shape);
		currentShape = shape;
	}
	
	public void endShape() {
		if (currentShape != 0) {
			glEnd();
			currentShape = 0;
		}
	}
	
	public void vertex(float x, float y) {
		v.setPosition(x, y);
		transform.applyTransformations(v);
		glVertex2f(v.x, v.y);
	}

}
