package com.adx.eng.render;

import com.adx.eng.frame.Game;
import com.adx.eng.io.TextFile;

import java.util.HashMap;

public class AngelCodeFont {

	public static final int H_ALIGN_LEFT = 0;
	public static final int H_ALIGN_CENTER = 1;
	public static final int H_ALIGN_RIGHT = 2;
	public static final int V_ALIGN_TOP = 0;
	public static final int V_ALIGN_MIDDLE = 1;
	public static final int V_ALIGN_BOTTOM = 2;
	
	private static HashMap<String, AngelCodeFont> mapFonts = new HashMap<String, AngelCodeFont>();

	private Texture[] aPages;
	private HashMap<Integer, CharDescriptor> mapChar = new HashMap<Integer, CharDescriptor>();

	private String name;
	private int lineHeight = 0;
	private int base = 0;
	private int pages = 0;

	private int hAlign = H_ALIGN_LEFT;
	private int vAlign = V_ALIGN_TOP;

	public class CharDescriptor {

		int character;
		int x;
		int y;
		int width;
		int height;
		int xOffset;
		int yOffset;
		int xAdvance;
		int page;
		Texture tex;
		float tx1;
		float ty1;
		float tx2;
		float ty2;

		public CharDescriptor(int character, int x, int y, int width, int height, int xOffset, int yOffset, int xAdvance, int page, Texture tex) {
			this.character = character;
			this.x = x;
			this.y = y;
			this.width = width;
			this.height = height;
			this.xOffset = xOffset;
			this.yOffset = yOffset;
			this.xAdvance = xAdvance;
			this.page = page;
			this.tex = tex;
			Texture t = aPages[page];
			tx1 = this.x / (float) t.getWidth();
			ty1 = this.y / (float) t.getHeight();
			tx2 = (x + width) / (float) t.getWidth();
			ty2 = (y + height) / (float) t.getHeight();
		}

	}

	public AngelCodeFont(String path, String name) {
		
		this.name = name;
		
		if (!mapFonts.containsKey(path + "/" + name)) {
			
			Game.postString("Loading font " + path + "/" + name + "...");
			TextFile file = new TextFile(path + "/" + name + ".fnt");
			if (!file.isExisting()) {
				Game.postWarning("Font " + path + "/" + name + " was not found.");
				return;
			}
	
			String line;
			String[] args;
			while ((line = file.readLine()) != null) {
				// Splits on 1-or-more spaces and = characters
				args = line.split("\\s+|=");
				if (args[0].equals("page")) {
					int page = Integer.parseInt(args[2]);
					String pagePath = args[4].replace("\"", "");
					aPages[page] = new Texture(path + "/" + pagePath);
				} else if (args[0].equals("common")) {
					lineHeight = Integer.parseInt(args[2]);
					base = Integer.parseInt(args[4]);
					pages = Integer.parseInt(args[10]);
					aPages = new Texture[pages];
				} else if (args[0].equals("char")) {
					int character = Integer.parseInt(args[2]);
					int x = Integer.parseInt(args[4]);
					int y = Integer.parseInt(args[6]);
					int width = Integer.parseInt(args[8]);
					int height = Integer.parseInt(args[10]);
					int xOffset = Integer.parseInt(args[12]);
					int yOffset = Integer.parseInt(args[14]);
					int xAdvance = Integer.parseInt(args[16]);
					int page = Integer.parseInt(args[18]);
					mapChar.put(character, new CharDescriptor(character, x, y, width, height, xOffset, yOffset, xAdvance, page, aPages[page]));
				}
			}
			
			mapFonts.put(path + "/" + name, this);
			
		} else {
			
			AngelCodeFont f = mapFonts.get(path + "/" + name);
			lineHeight = f.lineHeight;
			base = f.base;
			pages = f.pages;
			aPages = f.aPages;
			mapChar = f.mapChar;

		}
		
	}

	public String getName() {
		return name;
	}

	public int getLineHeight() {
		return lineHeight;
	}

	public int getBase() {
		return base;
	}

	public HashMap<Integer, CharDescriptor> getCharacterMap() {
		return mapChar;
	}

	public void setAlign(int hAlign, int vAlign) {
		this.hAlign = hAlign;
		this.vAlign = vAlign;
	}

	public void draw(GraphicsRenderer g, String text, float x, float y) {

		int c;
		CharDescriptor d;
		float xDraw = x;
		float yDraw = y;
		int lineWidth;

		String strDraw;
		String[] split = text.split("\\n");

		if (vAlign == V_ALIGN_BOTTOM) {
			yDraw -= split.length * lineHeight;
		} else if (vAlign == V_ALIGN_MIDDLE) {
			yDraw -= (split.length * lineHeight) / 2;
		}

		for (int i = 0; i < split.length; i++) {

			strDraw = split[i];
			if (hAlign != H_ALIGN_LEFT) {
				lineWidth = getWidth(strDraw);
				if (hAlign == H_ALIGN_RIGHT) {
					xDraw -= lineWidth;
				} else if (hAlign == H_ALIGN_CENTER) {
					xDraw -= lineWidth / 2;
				}
			}
			for (int j = 0; j < strDraw.length(); j++) {
				c = (int) strDraw.charAt(j);
				d = mapChar.get(c);
				if (d != null) {
					g.drawTextureSizedPartial(d.tex, xDraw + d.xOffset, yDraw + d.yOffset, d.width, d.height, d.tx1, d.ty1, d.tx2, d.ty2);
					xDraw += d.xAdvance;
				}
			}
			xDraw = x;
			yDraw += lineHeight;

		}

	}

	public int getWidth(String str) {
        String[] split = str.split("\\n");
        int maxWidth = 0;
        int width;
        for (String s : split) {
            width = getRawWidth(s);
            if (width > maxWidth) {
                maxWidth = width;
            }
        }
        return maxWidth;
	}

    public int getRawWidth(String str) {
        int c;
        CharDescriptor d;
        int width = 0;
        for (int i = 0; i < str.length(); i++) {
            c = (int) str.charAt(i);
            d = mapChar.get(c);
            if (d != null) {
                width += d.xAdvance;
            }
        }
        return width;
    }
	
	public int getHeight(String str) {
		String[] split = str.split("\\n");
		return split.length * lineHeight;
	}

}